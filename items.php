<?
$items=array(
  "unixtime" => array(
         "desc" => "Дата публикации",
         "type" => "date",
         "fields" => array(
                      "d" => array("name" => "День","size" => "2", "after" => "."),
                      "m" => array("name" => "Месяц","size" => "2", "after" => "."),
                      "Y" => array("name" => "Год","size" => "4"),
                      "H" => array("name" => "Часы","size" => "2", "after" => ":"),
                      "i" => array("name" => "Минуты","size" => "2"),
                      ),
         "select_on_edit" => true,
         "select_on_edit_disabled" => true,
       ),
  "name" => array(
         "desc" => "Название фотографии",
         "full_desc" => "Для страницы с подробным содержанием",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
       ),
  "descrip" => array(
         "desc" => "Описание фотографии",
         "type" => "editor",
         "width" => "700",
         "height" => "500",
         "select_on_edit" => true,
       ),



  "photo" => array
  (
    "type"                  => "processed_image",
    "file_is"               => "processed_image",
    "desc"                  => "Фотография",
    "full_desc"             => "Только изображения форматов JPEG, GIF и PNG",
    "store_source_name"     => true,          // сохранять ли имя загруженного файла, которое тот имеет на компьютере пользователя?
    "store_dimensions"      => true,          // хранить ли в БД размеры картинки, или определять их из файла (false)
    "check_file"            => true,          // проверять ли при отображении картинки существование файла, или доверять информации из БД (false)
    "check_dimensions"      => true,          // проверять ли при отображении картинки её размеры, или доверять информации из БД (false)
    "lt_show_system"        => true,          // разрешает отображать lt_source и lt_preview
    "lt_delete_files"       => true,          // разрешает возможность удалять файлы картинок для данной записи
    "lt_allow_name_change"  => false,          // разрешает загружать файл под именем, указанным пользователем
    "lt_preview"            => "small",        // индекс картинки, которая считается картинкой для предварительного просмотра
    "lt_allow_remote_files" => true,

    "images" => array
    (
      "small" => array
      (
        "prefix"  => "_",
        "target_type" => 2,
        "transforms" => array
        (
          "resize" => array("w"=>130 ,"h"=>0),
        ),
        "quality" => 85
      ),

      "big" => array
      (
        "prefix" => "big_",
        "target_type" => 2,
        "transforms" => array
        (
          "resize" => array("w" => 640 , "h" => 0),
        ),
        "quality" => 100,
      ),
    ),
    "select_on_edit" => true,
  ),
);
?>